﻿using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Model
{
    public interface IEmployee
    {
        int AppliedPromocodesCount { get; set; }
        string Email { get; set; }
        string FirstName { get; set; }
        string FullName { get; }
        string LastName { get; set; }
        List<Role> Roles { get; set; }
    }
}