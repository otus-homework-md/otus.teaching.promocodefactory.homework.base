﻿namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Model
{
    public interface IRole
    {
        string Description { get; set; }
        string Name { get; set; }
    }
}