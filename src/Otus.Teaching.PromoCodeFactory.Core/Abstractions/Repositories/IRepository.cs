﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;

public interface IRepository<T>
    where T: BaseEntity
{
    Task<IEnumerable<T>> GetAllAsync();
    
    Task<T> GetByIdAsync(Guid id);

    Task<bool> CreateAsync(T createEmployeeModel);
    Task<bool> UpdateAsync(T updateEmployeeModel);
    Task<bool> DeleteByIdAsync(Guid id);   
}