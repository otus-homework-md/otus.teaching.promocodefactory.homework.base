﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Model;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role
        : BaseEntity, IRole
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}