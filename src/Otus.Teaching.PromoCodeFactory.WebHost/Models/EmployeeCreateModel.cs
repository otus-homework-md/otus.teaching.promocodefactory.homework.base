﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeCreateModel
    {
        //public Guid Id { get; set; }    = Guid.NewGuid();
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Email { get; set; }
        //public List<RoleItemResponse> Roles { get; set; }

        public int AppliedPromocodesCount { get; set; } = 0;
    }
}