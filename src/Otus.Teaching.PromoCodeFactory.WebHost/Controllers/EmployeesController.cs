﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IMapper _autoMapper;

        public EmployeesController(IRepository<Employee> employeeRepository, IMapper mapper)
        {
            _employeeRepository = employeeRepository;
            _autoMapper = mapper;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Удалить сотрудника из репозитория по Id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<bool>> DeleteEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var res = await _employeeRepository.DeleteByIdAsync(id);

            return res;
        }

        /// <summary>
        /// Добавить сотрудника в репозиторий
        /// </summary>
        /// <param name="employeeCreateModel"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<bool>> AddEmployeeAsync([FromBody] EmployeeCreateModel employeeCreateModel)
        {
            var employee = _autoMapper.Map<Employee>(employeeCreateModel);
            employee.Id = Guid.NewGuid();
            employee.Roles = new List<Role>();
            var result=await _employeeRepository.CreateAsync(employee);
            return result;
        }

        /// <summary>
        /// Обновить данные по сотруднику в репозитории
        /// </summary>
        /// <param name="employeeCreateModel"></param>
        /// <returns></returns>
        [HttpPatch]
        public async Task<ActionResult<bool>> UpdateEmployeeAsync([FromBody] EmployeeUpdateModel employeeUpdateModel)
        {
            var employee = _autoMapper.Map<Employee>(employeeUpdateModel);
            
            employee.Roles = ((await _employeeRepository.GetByIdAsync(employeeUpdateModel.Id)) as Employee).Roles ;
            var result = await _employeeRepository.UpdateAsync(employee);
            return result;
        }


    }
}