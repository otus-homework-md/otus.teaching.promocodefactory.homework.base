﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Models.EmployeeCreateModel, Employee>();
            CreateMap<Employee, Models.EmployeeCreateModel>();
            CreateMap<Models.EmployeeUpdateModel, Employee>();
            CreateMap<Employee, Models.EmployeeUpdateModel>();
            CreateMap<Models.RoleItemResponse, Role>();
            CreateMap<Role, Models.RoleItemResponse>();
        }
    }
}
